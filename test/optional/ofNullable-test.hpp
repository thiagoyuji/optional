#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void ofNullableInputAbstractValuePointer(){
	class Test{ public: int const x = 10; };
	assert(Optional<Test *>::ofNullable(new Test()).get().x == 10);
	std::cout << "[TEST]::[ofNullableInputAbstractValuePointer]::SUCCESS" << std::endl;
}

void ofNullableInputAbstractValuePointerNull(){
	class Test{ public: int const x = 10; };
	assert(Optional<Test *>::ofNullable(nullptr).isEmpty());
	std::cout << "[TEST]::[ofNullableInputAbstractValuePointerNull]::SUCCESS" << std::endl;
}

void ofNullableInputPrimitiveValuePointer(){
	assert(Optional<int *>::ofNullable(new int(10)).get() == 10);
	assert(Optional<float *>::ofNullable(new float(2.5)).get() == 2.5);
	assert(Optional<char *>::ofNullable(new char('a')).get() == 'a');
	assert(Optional<double *>::ofNullable(new double(5.5)).get() == 5.5);
	assert(Optional<std::string *>::ofNullable(new std::string("ola")).get() == "ola");
	assert(Optional<bool *>::ofNullable(new bool(true)).get());
	std::cout << "[TEST]::[ofNullableInputPrimitiveValuePointer]::SUCCESS" << std::endl;
}

void ofNullableInputPrimitiveValuePointerNull(){
	assert(Optional<int *>::ofNullable(nullptr).isEmpty());
	std::cout << "[TEST]::[ofNullableInputPrimitiveValuePointerNull]::SUCCESS" << std::endl;
}