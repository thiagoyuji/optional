#include "of-test.hpp"
#include "ofNullable-test.hpp"
#include "get-test.hpp"
#include "isEmpty-test.hpp"
#include "isPresent-test.hpp"
#include "orElse-test.hpp"
#include "orElseGet-test.hpp"
#include "orElseThrow-test.hpp"
#include "filter-test.hpp"
#include "map-test.hpp"
#include "ifPresent-test.hpp"
#include "ifNotPresent-test.hpp"
#include "execute-test.hpp"

int main(){

    ofInputPrimitiveValue();
    ofInputAbstractValue();
    ofInputPrimitiveValuePointerNull();
    ofInputPrimitiveValuePointer();
    ofInputAbstractValuePointerNull();
    ofInputAbstractValuePointer();

    ofNullableInputPrimitiveValuePointerNull();
    ofNullableInputPrimitiveValuePointer();
    ofNullableInputAbstractValuePointerNull();
    ofNullableInputAbstractValuePointer();

    getPrimitiveValuePointerNull();
    getAbstractValuePointerNull();
    getPointerPrimitiveValuePointerNull();
    getPointerPrimitiveValuePointer();
    getPointerAbstractValuePointerNull();
    getPointerAbstractValuePointer();

    isEmpty();
    isNotEmpty();

    isPresent();
    isNotPresent();

    orElsePrimitiveValue();
    orElsePrimitiveValuePointerReturningPointerNull();
    orElsePrimitiveValuePointerReturningPrimitive();
    orElsePrimitiveValuePointerReturningPointer();
    orElseAbstractValuePointerReturningPointerNull();
    orElseAbstractValuePointerReturningAbstract();
    orElseAbstractValuePointerReturningPointer();

    orElseGetPrimitiveValueProviderNull();
    orElseGetPrimitiveValue();
    orElseGetPrimitiveValueProviderNull();
    orElseGetPointerPrimitiveValueReturningPrimitiveProviderNull();
    orElseGetPrimitiveValueReturningPrimitive();
    orElseGetPrimitiveValueReturningPointer();
    orElseGetAbstractValueProviderNull();
    orElseGetPointerAbstractValueProviderNull();
    orElseGetAbstractValueReturningAbstract();
    orElseGetAbstractValueReturningPointer();

    orElseThrowPrimitiveProviderNull();
    getPointerOrElseThrowPrimitiveProviderNull();
    orElseThrowAbstractProviderNull();
    getPointerOrElseThrowAbstractProviderNull();
    orElseThrowPrimitiveThrow();
    getPointerOrElseThrowPrimitiveThrow();
    orElseThrowAbstractThrow();
    getPointerOrElseThrowAbstractThrow();
    orElseThrowPrimitiveGet();
    orElseThrowAbstractGet();

    filterPrimitivePointerPredicateNull();
    filterAbstractPointerPredicateNull();
    filterPrimitivePointerIsPresentTrueAndPredicateTrue();
    filterPrimitivePointerIsPresentFalseAndPredicateFalse();
    filterPrimitivePointerIsPresentTrueAndPredicateFalse();
    filterAbstractPointerIsPresentTrueAndPredicateTrue();
    filterAbstractPointerIsPresentFalseAndPredicateFalse();
    filterAbstractPointerIsPresentTrueAndPredicateFalse();

    mapPrimitiveMapperNull();
    mapPrimitiveMapper();
    mapAbstractMapperNull();
    mapAbstractMapper();
    mapPrimitivePointerMapperNull();
    mapPrimitivePointerValuePresent();
    mapPrimitivePointerValueNotPresent();
    mapAbstractPointerMapperNull();
    mapAbstractPointerValuePresent();
    mapAbstractPointerValueNotPresent();

    ifPresentPrimitivePointerConsumerNull();
    ifPresentPrimitivePointerConsumer();
    ifPresentAbstractPointerConsumerNull();
    ifPresentAbstractPointerConsumer();
    ifNotPresent();

    ifNotPresentPrimitivePointerActionNull();
    ifNotPresentPrimitivePointerAction();
    ifNotPresentAbstractPointerActionNull();
    ifNotPresentAbstractPointerAction();
    ifPresent();

    executePrimitiveConsumerNull();
    executePrimitiveConsumer();
    executeAbstractConsumerNull();
    executeAbstractConsumer();

    return 0;

}