#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void getAbstractValuePointerNull(){
	class Test{ public: int const x = 10; };
	try{
		Optional<Test *>::empty().get();
		assert(false);
	}catch (const nullptr_value &exception){
		assert(true);
		std::cout << "[TEST]::[getAbstractValuePointerNull]::SUCCESS::" << exception.what() << std::endl;
	}
}

void getPrimitiveValuePointerNull(){
	try{
		Optional<int *>::empty().get();
		assert(false);
	}catch (const nullptr_value &exception){
		assert(true);
		std::cout << "[TEST]::[getPrimitiveValuePointerNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void getPointerPrimitiveValuePointerNull(){
	try{
		Optional<int *>::empty().getPointer();
		assert(false);
	}catch (const nullptr_value &exception){
		assert(true);
		std::cout << "[TEST]::[getPointerPrimitiveValuePointerNull]::SUCCESS::" << exception.what() << std::endl;
	};
}

void getPointerPrimitiveValuePointer(){
	assert(*Optional<int *>::of(new int(10)).getPointer() == 10);
	assert(*Optional<float *>::of(new float(2.5)).getPointer() == 2.5);
	assert(*Optional<char *>::of(new char('a')).getPointer() == 'a');
	assert(*Optional<double *>::of(new double(5.5)).getPointer() == 5.5);
	assert(*Optional<std::string *>::of(new std::string("ola")).getPointer() == "ola");
	assert(*Optional<bool *>::of(new bool(true)).getPointer());
	std::cout << "[TEST]::[getPointerPrimitiveValuePointer]::SUCCESS" << std::endl;
}

void getPointerAbstractValuePointerNull(){
	class Test{ public: int const x = 10; };
	try{
		Optional<Test *>::empty().getPointer();
		assert(false);
	}catch (const nullptr_value &exception){
		assert(true);
		std::cout << "[TEST]::[getPointerAbstractValuePointerNull]::SUCCESS::" << exception.what() << std::endl;
	}
}
void getPointerAbstractValuePointer(){
	class Test{ public: int const x = 10; };
	assert(Optional<Test *>::of(new Test()).getPointer()->x == 10);
	std::cout << "[TEST]::[getPointerAbstractValuePointer]::SUCCESS" << std::endl;
}