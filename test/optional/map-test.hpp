#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void mapPrimitiveMapperNull(){
    try{
        Optional<int>::of(10).map<int>(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[mapPrimitiveMapperNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void mapPrimitiveMapper(){
    assert(Optional<std::string>::of("123").map<int>([](std::string const value) { return std::stoi(value); }).get() == 123);
    assert(Optional<std::string>::of("123").map<int *>([](std::string const value) { return new int(std::stoi(value)); }).get() == 123);
    std::cout << "[TEST]::[mapPrimitiveMapper]::SUCCESS" << std::endl;
}

void mapAbstractMapperNull(){
    class Test{ public: int const x = 10; };
    try{
        Optional<Test>::of(Test()).map<int>(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[mapAbstractMapperNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void mapAbstractMapper(){
    class Test{ public: std::string const x = "10"; };
    assert(Optional<Test>::of(Test()).map<int>([](Test const value) { return std::stoi(value.x); }).get() == 10);
    assert(Optional<Test>::of(Test()).map<int *>([](Test const value) { return new int(std::stoi(value.x)); }).get() == 10);
    std::cout << "[TEST]::[mapAbstractMapper]::SUCCESS" << std::endl;
}

void mapPrimitivePointerMapperNull(){
    try{
        Optional<int *>::of(new int(10)).map<int>(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[mapPrimitivePointerMapperNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void mapPrimitivePointerValuePresent(){
    assert(Optional<std::string *>::of(new std::string("123"))
               .map<int>([](std::string const *const value) { return std::stoi(*value); })
               .get() == 123);
    assert(Optional<std::string *>::of(new std::string("123"))
               .map<int *>([](std::string const *const value) { return new int(std::stoi(*value)); })
               .get() == 123);
    std::cout << "[TEST]::[mapPrimitivePointerValuePresent]::SUCCESS" << std::endl;
}

void mapPrimitivePointerValueNotPresent(){
    assert(Optional<std::string *>::ofNullable(nullptr)
               .map<int>([](std::string const *const value) { return std::stoi(*value); })
               .get() == 0);
    assert(Optional<std::string *>::empty()
               .map<int *>([](std::string const *const value) { return new int(std::stoi(*value)); })
               .isEmpty());
    std::cout << "[TEST]::[mapPrimitivePointerValueNotPresent]::SUCCESS" << std::endl;
}

void mapAbstractPointerMapperNull(){
    class Test{ public: int const x = 10; };
    try{
        Optional<Test *>::of(new Test()).map<int>(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[mapAbstractPointerMapperNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void mapAbstractPointerValuePresent(){
    class Test{ public: std::string const x = "10"; };
    assert(Optional<Test *>::of(new Test())
               .map<int>([](Test const *const value) { return std::stoi(value->x); })
               .get() == 10);
    assert(Optional<Test *>::of(new Test())
               .map<int *>([](Test const *const value) { return new int(std::stoi(value->x)); })
               .get() == 10);
    std::cout << "[TEST]::[mapAbstractPointerValuePresent]::SUCCESS" << std::endl;
}

void mapAbstractPointerValueNotPresent(){
    class Test{ public: std::string const x = "10"; };
    assert(Optional<Test *>::ofNullable(nullptr)
               .map<int>([](Test const *const value) { return std::stoi(value->x); })
               .get() == 0);
    assert(Optional<Test *>::empty()
               .map<int *>([](Test const *const value) { return new int(std::stoi(value->x)); })
               .isEmpty());
    std::cout << "[TEST]::[mapAbstractPointerValueNotPresent]::SUCCESS" << std::endl;
}