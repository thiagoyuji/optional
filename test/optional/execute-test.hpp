#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void executePrimitiveConsumerNull(){
    try{
        Optional<int>::of(10).execute(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[executePrimitiveConsumerNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void executePrimitiveConsumer(){
    int sum = 10;
    Optional<int>::of(10).execute([&sum](int const value) { sum += value; });
    assert(sum == 20);
    std::cout << "[TEST]::[executePrimitiveConsumer]::SUCCESS" << std::endl;
}

void executeAbstractConsumerNull(){
    class Test{ public: int const x = 10; };
    try{
        Optional<Test>::of(Test()).execute(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[executeAbstractConsumerNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void executeAbstractConsumer(){
    class Test{ public: int x = 10; };
    Test test = Test();
    int sum = 10;
    Optional<Test>::of(Test()).execute([&test](Test const value) { test.x += value.x; });
    assert(test.x == 20);
    std::cout << "[TEST]::[executeAbstractConsumer]::SUCCESS" << std::endl;
}