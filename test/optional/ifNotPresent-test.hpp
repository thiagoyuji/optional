#include <iostream>
#include <cassert>

#include "../../include/optional/optional.hpp"

void ifNotPresentPrimitivePointerActionNull(){
    try{
        Optional<int *>::ofNullable(nullptr).ifNotPresent(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[ifNotPresentPrimitivePointerActionNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void ifNotPresentPrimitivePointerAction(){
    int sum = 10;
    Optional<int *>::ofNullable(nullptr).ifNotPresent([&sum]() { sum += 10; });
    assert(sum == 20);
    std::cout << "[TEST]::[ifNotPresentPrimitivePointerAction]::SUCCESS" << std::endl;
}

void ifNotPresentAbstractPointerActionNull(){
    class Test{ public: int const x = 10; };
    try{
        Optional<Test *>::empty().ifNotPresent(nullptr);
        assert(false);
    }catch (const std::invalid_argument &exception){
        assert(true);
        std::cout << "[TEST]::[ifNotPresentAbstractPointerActionNull]::SUCCESS::" << exception.what() << std::endl;
    };
}

void ifNotPresentAbstractPointerAction(){
    class Test{ public: int x = 10; };
    Test test = Test();
    Optional<Test *>::empty().ifNotPresent([&test]() { test.x += 10; });
    assert(test.x == 20);
    std::cout << "[TEST]::[ifNotPresentAbstractPointerAction]::SUCCESS" << std::endl;
}

void ifPresent(){
    class Test{ public: int x = 10; };
    int sum = 10;
    Optional<int *>::of(new int(10)).ifNotPresent([&sum]() { sum += 10; });
    Optional<int *>::of(new int(10)).ifNotPresent([&sum]() { sum += 10; });
    Test test = Test();
    Optional<Test *>::of(new Test()).ifNotPresent([&test]() { test.x += 10; });
    Optional<Test *>::of(new Test()).ifNotPresent([&test]() { test.x += 10; });
    assert(sum == 10);
    assert(test.x = 10);
    std::cout << "[TEST]::[ifPresent]::SUCCESS" << std::endl;
}